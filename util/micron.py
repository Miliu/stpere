from __future__ import division
import numpy as np


def to_range(
    beams,
    threshold=100,
    min_range=0,
    max_range=100,
    min_distance=2,
    resolution=0.1,
    max_ranges=None
):
    """ Micron DST beams to ranges. segmentation according to the
    higher intensity returns.

    :param beams: beams
    :param thershold: segmentation thershold [0-255]
    :param min_range: minimum detection range [meters]
    :param max_range: maximum detection range [meters]
    :param min_distance: minimum distante between 2 range measurements
        on the same beam [meters]
    :param resolution: Micros DST linear resolution [meters]
    :param max_ranges: maximum number of ranges per beam.
        'None' takes all ranges
    :type beams: numpy 2d array [N x M]; N num beams; M beam intensities
    :type thershold: int
    :type min_range: float
    :type max_range: float
    :type min_distance: float
    :type resolution: float
    :type max_ranges: int

    :return: ranges per each input beam
    :rtype: list of list. [N x 0..max_ranges]
    """

    # parameters in cell units
    c_min = int(min_range / resolution)
    c_max = min(int(max_range / resolution), beams.shape[1])
    c_min_distance = int(min_distance / resolution)

    # very basic segmentation
    beams[:, 0:c_min] = 0  # avoids self-detection
    beams[:, c_max:] = 0  # crops signal
    beams[beams <= threshold] = 0

    ranges = []
    for b in beams:
        idx_candidates = np.nonzero(b)
        idx_sorted = b[idx_candidates].argsort()[::-1]
        idx_candidates = idx_candidates[0][idx_sorted]
        idx_local_max = np.ones(idx_sorted.shape)

        # indices that sasify min_distance condition
        count = 0
        for i in range(len(idx_candidates)-1):
            if idx_local_max[i]:
                count = count + 1
                if count == max_ranges:
                    idx_local_max[i+1:] = 0
                    break
                for j in range(i+1, len(idx_candidates)):
                    if abs(idx_candidates[i]-idx_candidates[j]) < c_min_distance:
                        idx_local_max[j] = 0

        # ranges in meters
        idx_local_max = np.nonzero(idx_local_max)[0]
        beam_range = idx_candidates[idx_local_max][0:len(idx_local_max)] * resolution
        ranges.append(beam_range)

    return ranges


def main():
    from itertools import islice
    import matplotlib.pyplot as plt

    fn = "../experiment3/_040825_1735_IS.log"
    num_registers = 10
    comments = "%"
    delimiter = " "
    with open(fn) as f:
        data = np.genfromtxt(
            islice(f, num_registers),
            comments=comments,
            delimiter=delimiter)

    mis_config = {
        "id": "micron",
        "pose": np.array([0.33, 0, -0.26, 0, 0, np.pi]),
        "stdev_linear": 0.1,
        "stdev_angular": 3*np.pi/180,
        "resolution_linear": 0.1,  # m
        "segmentation": {
            "threshold": 70,  # [0-255]
            "init_range": 1.5,  # m
            "max_range": 50,  # m
            "min_distance": 50,  # m
            "max_ranges": 2
            }
        }

    if len(data.shape) == 1:
        data = np.array([data])

    original = np.copy(data)
    ranges = to_range(
        data[:, 3:],
        threshold=mis_config["segmentation"]["threshold"],
        min_range=mis_config["segmentation"]["init_range"],
        max_range=mis_config["segmentation"]["max_range"],
        resolution=mis_config["resolution_linear"])
        # max_ranges=mis_config["segmentation"]["max_ranges"])


    fig = plt.figure()
    plt.imshow(original[:,3:], interpolation="nearest")

    fig = plt.figure()
    plt.imshow(data[:,3:], interpolation="nearest")

    print ranges
    x_val = []
    y_val = []
    for i, r in enumerate(ranges):
        for j in r:
            y_val.append(i)
            x_val.append(j / mis_config["resolution_linear"])
    print "X, Y", x_val, y_val



    plt.scatter(x_val, y_val, c="g", marker=".", s=60)


    # fig = plt.figure()
    # plt.imshow(seg)

    plt.show()

    print "Done"

if __name__ == "__main__":
    main()
