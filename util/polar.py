import numpy as np

"""Polar - Cartesian convertions"""


def pol_to_cart(rho, phi):
    """ polar to cartesian"""
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)


def cart_to_pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)
